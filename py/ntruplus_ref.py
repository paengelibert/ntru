import subprocess

CLI_PATH = "../ntruplus_cli/ntruplus576"

# -> (pk, sk)
def keygen():
	p = subprocess.run([CLI_PATH, "g"], capture_output=True)
	return (p.stdout[:864], p.stdout[864:2624])

# -> (ciphertext, shared_secret)
def encaps(pk):
	p = subprocess.Popen([CLI_PATH, "e"], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
	out = p.communicate(input=pk)[0]
	return (out[:864], out[864:896])

# -> shared_secret
def decaps(ciphertext, sk):
	p = subprocess.Popen([CLI_PATH, "d"], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
	out = p.communicate(input=ciphertext+sk)[0]
	return out[:32]
