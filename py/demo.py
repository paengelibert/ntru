import hashlib
round_ = round
from sage.all import *
round = round_
import random

x = var("x")

def keygen(n, q):
	Fq = PolynomialRing(GF(q), "x")
	Psi1 = PolynomialRing(GF(2), "x")
	xf = Fq(x)
	Rq = Fq.quotient(xf**n - xf**(n // 2) + 1)
	while True:
		f_prime = Psi1.random_element(n)
		g = Psi1.random_element(n)
		f = 3 * Rq(f_prime) + 1
		try:
			f_inv = Rq(f)**(-1)
			g_inv = Rq(g)**(-1) # only for checking that g is invertible
			return (3 * Rq(g) * f_inv, f)
		except ZeroDivisionError:
			continue

# Operates lists
def sotp(n, x, u):
	return [x[i].__xor__(u[i]) - u[n + i] for i in range(n)]

# Operates lists
def sotp_inv(n, y, u):
	# The & 1 prevents xoring non-binary values but silently enables the attack
	return [(int(y[i] + u[n + i]) & 1).__xor__(u[i]) for i in range(n)]

# Only for n=576
def poly_to_bytes(a):
	a = [int(i) for i in list(a)]
	r = [0] * 864
	t = [0, 0, 0, 0]
	try:
		for i in range(16):
			for j in range(9):
				t[0] = a[64 * j + i]
				t[1] = a[64 * j + i + 16]
				t[2] = a[64 * j + i + 32]
				t[3] = a[64 * j + i + 48]
			
				r[96 * j + 2 * i] = t[0]
				r[96 * j + 2 * i + 1] = (t[0] >> 8) + (t[1] << 4)
				r[96 * j + 2 * i + 32] = (t[1] >> 4)
				r[96 * j + 2 * i + 33] = t[2]
				r[96 * j + 2 * i + 64] = (t[2] >> 8) + (t[3] << 4)
				r[96 * j + 2 * i + 65] = (t[3] >> 4)
	except:
		r = [0] * (len(a) * 3 // 2)
		for i in range(len(r)):
			r[i] = a[i%len(a)]%256
		return r
	return [i % 256 for i in r]

# Only for n=576
def poly_from_bytes(a):
	r = [0] * 576
	t = [0, 0, 0, 0, 0, 0]
	for i in range(16):
		for j in range(9):
			t[0] = a[96 * j + 2 * i]
			t[1] = a[96 * j + 2 * i + 1]
			t[2] = a[96 * j + 2 * i + 32]
			t[3] = a[96 * j + 2 * i + 33]
			t[4] = a[96 * j + 2 * i + 64]
			t[5] = a[96 * j + 2 * i + 65]
			
			r[64 * j + i] = t[0]
			r[64 * j + i] += (t[1] & 0xf) << 8
			r[64 * j + i + 16] = t[1] >> 4
			r[64 * j + i + 16] += t[2] << 4
			r[64 * j + i + 32] = t[3]
			r[64 * j + i + 32] += (t[4] & 0xf) << 8
			r[64 * j + i + 48] = t[4] >> 4
			r[64 * j + i + 48] += t[5] << 4
	return r

def H(l, m):
	h = hashlib.sha512(bytes(m)).digest()
	while len(h) < 2 * l:
		h *= 2
	return ([i % 2 for i in h[:l]], [i % 2 for i in h[l:2 * l]])

def G(l, m):
	h = hashlib.sha512(bytes([42, 42, 42] + m + [6, 6, 6])).digest()
	while len(h) < 2 * l:
		h *= 2
	return [i % 2 for i in h[:2 * l]]

def tern_to_center(x):
	return [(-1 if (i == 2) else i) for i in x]

def modq_to_center(q, x):
	q2 = q // 2
	return [((int(i) - q) if (int(i) > q2) else int(i)) for i in x]

def encaps(n, q, pk):
	Fq = PolynomialRing(GF(q), "x")
	xf = Fq(x)
	Rq = Fq.quotient(xf**n - xf**(n // 2) + 1)
	m = [random.randint(0, 1) for i in range(n)]
	r, K = H(n, m)
	# print("r=", r)
	M = sotp(n, m, G(864, poly_to_bytes(r)))
	# print("M= ", M)
	c = Rq(pk) * Rq(r) + Rq(M)
	return (K, c)

def decaps(n, q, pk, sk, c):
	Fq = PolynomialRing(GF(q), "x")
	xf = Fq(x)
	Rq = Fq.quotient(xf**n - xf**(n // 2) + 1)
	M = tern_to_center([int(i) % 3 for i in modq_to_center(q, list(c * sk))])
	# print("M= ", M)
	r = (c - Rq(M)) * pk**(-1)
	m = sotp_inv(n, M, G(864, poly_to_bytes(list(r))))
	r_prime, K = H(n, m)
	# print("r= ", list(r))
	# print("r'= ", r_prime)
	if list(r) != r_prime:
		return None
	return K

def demo_usage(n, q):
		Fq = PolynomialRing(GF(q), "x")
		xf = Fq(x)
		print("### pk, sk = keygen(n, q)")
		pk, sk = keygen(n, q)
		print("pk =", list(pk))
		print("sk =", list(sk))
		print()
		print("### K1, c = encaps(n, q, pk)")
		K1, c = encaps(n, q, pk)
		print("c  =", list(c))
		print("K1 =", K1)
		print()
		print("### K2 = decaps(n, q, pk, sk, c)")
		K2 = decaps(n, q, pk, sk, c)
		print("K2 =", K2)
		print()
		print("### K3 = decaps(n, q, pk, sk, c + 2X)")
		K3 = decaps(n, q, pk, sk, c+2*xf)
		print("K3 =", K3)

def demo_proba(n, q, iters):
	Fq = PolynomialRing(GF(q), "x")
	xf = Fq(x)
	wins = 0
	fails = 0
	for i in range(1000):
		pk, sk = keygen(n, q)
		K1, c = encaps(n, q, pk)

		# Attack, yaaarr!
		win = False
		for i in range(iters):
			c2 = c + 2 * xf**i
			K2 = decaps(n, q, pk, sk, c2)
			if K1 == K2:
				win = True
				break
		if win:
			wins += 1
		else:
			fails += 1
		print("wins=", wins, " fails=", fails, end="\r")
	print("wins=", wins, " fails=", fails)
	print("Attack win rate: " + str(round(int(wins) / int(wins + fails) * 100, 4)) + " %")
	

if __name__ == "__main__":
	n = 5
	q = 53

	print("n =", n)
	print("q =", q)
	print()
	
	demo_usage(n, q)
	
	n = 576
	q = 3457

	print("n =", n)
	print("q =", q)
	print()
	
	demo_proba(n, q, n)
