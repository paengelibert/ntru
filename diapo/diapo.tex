\documentclass[landscape,dvipsnames,notheorems]{beamer}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage[main=french]{babel}
\usepackage{enumitem}
\usepackage[export]{adjustbox}
\usepackage{multirow}
\usepackage{pifont}
\usepackage{textcomp}
\usepackage{tikz}
\usepackage{ulem}
\usepackage{array}
\usepackage{xcolor}
\usepackage{cancel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathtools, stmaryrd}
\usepackage{amsthm}
\usepackage{libertine}
\usepackage{multirow}
% \usepackage{graphicx}
% \usepackage{subcaption}
% \usepackage[
%   backend=biber,
%   style=numeric,
%   %sorting=ynt
% ]{biblatex}
\usepackage{minted}
\usepackage{listings}
\usepackage{csquotes}

%\mode<presentation> { \setbeamercovered{transparent} }
%\setbeamertemplate{navigation symbols}{}
%\makeatletter
%\def\beamerorig@set@color{%
%  \pdfliteral{\current@color}%
%  \aftergroup\reset@color
%}
%\def\beamerorig@reset@color{\pdfliteral{\current@color}}
%\makeatother

\tikzstyle{level 1}=[level distance=3.5cm, sibling distance=2cm]
\tikzstyle{level 2}=[level distance=3.5cm, sibling distance=1cm]
\usetikzlibrary{trees,positioning,fit,arrows,decorations.pathreplacing}
\setbeamertemplate{footline}[frame number]

\renewcommand\mathfamilydefault{\rmdefault}
\renewcommand\CancelColor{\color{red}}

\DeclareMathOperator{\ppcm}{ppcm}
\DeclareMathOperator{\pgcd}{pgcd}
\DeclareMathOperator{\rem}{rem}
\DeclareMathOperator{\sotp}{SOTP}
\DeclareMathOperator{\inv}{Inv}
\DeclareMathOperator{\encrypt}{Encrypt}
\DeclareMathOperator{\decrypt}{Decrypt}
\DeclareMathOperator{\keygen}{KeyGen}
\DeclareMathOperator{\encaps}{Encaps}
\DeclareMathOperator{\decaps}{Decaps}

\DeclarePairedDelimiterX{\Iintv}[1]{\llbracket}{\rrbracket}{\iintvargs{#1}}
\NewDocumentCommand{\iintvargs}{>{\SplitArgument{1}{,}}m}
{\iintvargsaux#1} %
\NewDocumentCommand{\iintvargsaux}{mm} {#1\mkern1.5mu..\mkern1.5mu#2}

%\setlength{\parskip}{1em}

\setitemize{
	label=\ding{226}\enskip,
}

\title[Attaque à chiffré choisi sur NTRU+]{Attaque à chiffré choisi sur NTRU+}
\date{17 Avril 2024}
\author[Paul Capgrand, Pascal Engélibert]{Paul Capgrand, Pascal Engélibert}
\institute[Université de Bordeaux]{Université de Bordeaux}

% \addbibresource{diapo.bib}

% \titlegraphic{\includegraphics[height=.4\textheight]{hdd.jpg}}

\tikzset{
	every overlay node/.style={
		anchor=south east,
	},
}
% Usage:
% \tikzoverlay at (-1cm,-5cm) {content};
% or
% \tikzoverlay[text width=5cm] at (-1cm,-5cm) {content};
\def\tikzoverlay{
	\tikz[baseline,overlay]\node[every overlay node]
}

\begin{document}

\maketitle
\begin{frame}{Introduction}{Un peu de contexte}
	\begin{itemize}[label={\textbullet}]
		\item Article publié par Lee et al dans le cadre de la KpqC (débutée en 2022)\\ \pause
		\item Attaque de NTRU$+$ qui casse la sécurité OW-CCA\\ \pause
		\item NTRU$+$ est un KEM (Key Encapsulation Mechanism)
	\end{itemize}
\end{frame}

\begin{frame}{Sommaire}{Sommaire}
	\begin{itemize}
		\item NTRU
		\item KEM et NTRU+
		\item Attaque et contre-mesure
		\item Implémentations
	\end{itemize}
\end{frame}

\begin{frame}{NTRU-HPS}{Génération des clés}
	Proposé en 1996 par Hoffstein, Pipher et Silverman.\\ Système de chiffrement à clé publique utilisant des polynômes tronqués.\\ \pause
	\begin{itemize}[label={\textbullet}]
		\item $N, t, p, q \text{ avec } p,q \text{ premiers entre eux}$ \pause
		\item $f,g\in\mathcal{R}=\mathbb{Z}[X] / (X^N - 1) \text{ à coefficients dans } \{-1,0,1\}$ \pause
		\item $f_q = f^{-1}\mkern-15mu\mod q  \quad\text{ et } \quad f_3 = f^{-1}\mkern-15mu\mod 3$ \pause
		\item $pk = h = 3f_q\cdot g \mod q$ \pause
		\item $sk = f$
	\end{itemize}
\end{frame}

\begin{frame}{NTRU-HPS}{Chiffrement \& Déchiffrement}
	Encrypt(h,m) $\rightarrow$ c
	\begin{itemize}[label={\textbullet}]
		\item $m\in\mathcal{R} \text{ à coefficients dans } \{-1,0,1\}$
		\item $r\in\mathcal{R} \text{ à coefficients dans } \{-1,0,1\} \text{ aléatoire de petit poids}$
		\item $c=r\cdot h+m \mod q$ \pause
	\end{itemize}
\vspace{15pt}
	Decrypt(f,c) $\rightarrow$ m
	\begin{itemize}[label={\textbullet}]
		\item $a=f\cdot c \mod q$
		\item $b=a \mod 3$
		\item $m=b\cdot f_3 \mod 3$
	\end{itemize}

\end{frame}

\begin{frame}{NTRU-HPS}{Correct}
	\begin{itemize}[label={\textbullet}]
		\item $a = f \cdot c \mod q$\pause
		\item $a = f \cdot (r \cdot h + m) \mod q$\pause
		\item $a = f \cdot (r \cdot 3f_q \cdot g + m) \mod q$\pause
		\item $a = 3r \cdot g + f \cdot m \mod q$\pause
		\item $b = f \cdot m \mod 3$\pause
		\item $f_3 \cdot b = f_3 \cdot f \cdot m \mod 3$\pause
		\item $f_3 \cdot b = m \mod 3$ \pause
	\end{itemize}
	\vspace{15pt}
	\begin{center}
		$f \cdot c = m \mod 3$ si $f_3 = 1$
	\end{center}
\end{frame}

\begin{frame}{Mécanisme d'Échange de Clés (KEM)}
	\begin{tikzpicture}
		\node (A) at (-4,0) {Anatole};
		\node (B) at (4,0) {Bernadette};
		\node (A1) at (-4,-1) {$(pk,sk)=\keygen()$};
		\node (A1t) at (-2,-1.5) {};
		\node (B1) at (4,-1) {};
		\node (B1t) at (2,-1.5) {};
		\node (A2) at (-4,-2) {};
		\node (A2t) at (-2,-2.5) {};
		\node (B2) at (4,-2) {$(K,c)=\encaps(pk)$};
		\node (B2t) at (2,-2.5) {};
		\node (A3) at (-4,-3) {$K=\decaps(sk,c)$};
		\node (B3) at (4,-3) {};
		\path [->](A1t) edge node[above] {$pk$} (B1t);
		\path [->](B2t) edge node[above] {$c$} (A2t);
	\end{tikzpicture}
\end{frame}

\begin{frame}{NTRU+}{Génération des clés}
	\begin{itemize}[label={\textbullet}]
		\item Générer $f'$ et $g$ $\in\mathcal{R}=\mathbb{Z}[X] / (X^n - X^{n/2} + 1)$\pause
		\item $f = 3f' + 1$\pause
		\item Si les inverses de f et g modulo $q$ n'existent pas, recommencer.\pause
		\item $(pk,sk) = (h = 3g \cdot f^{-1} \text{mod } q,f)$
	\end{itemize}
\end{frame}

\begin{frame}{NTRU+}{Encapsulation et Decapsulation}
Encaps$(pk)\rightarrow(K,c)$
\begin{itemize}[label={\textbullet}]
	\item Générer $m$ un n-uplet à valeur dans $\{0,1\}$\pause
	\item $(r,K) = H(m)$ avec $H : \{0,1\}^{*} \rightarrow \{0,1\}^{n} \times \mathcal{K}$\pause
	\item $M = \text{SOTP}(m,G(r))$ avec $G : \{0,1\}^{*} \rightarrow \{0,1\}^{2n}$\pause
	\item $c = h \cdot r + M$ (mod $q$)\pause
\end{itemize}
\vspace{15pt}
Decaps$(sk,c)\rightarrow K \text{ ou } \bot$
\begin{itemize}[label={\textbullet}]
	\item $M = (c \cdot f \mod q) \mod 3$\pause
	\item $r = (c - M) \cdot h^{-1} \mod q$\pause
	\item $m = \inv(M,G(r))$\pause
	\item $(r',K) = H(m)$\pause
	\item si $r = r'$ retourner $K$, sinon retourner $\bot$.
\end{itemize}
\end{frame}

\begin{frame}{NTRU+}{OW-CCA}
	\begin{center}
		Couple BUT-MOYEN de l'attaquant \pause
	\end{center}
	\vspace{15pt}
	BUTS : 
	\begin{itemize}[label={\textbullet}]
		\item IND~: Indistinguabilité \pause
		\item OW~: One-Way \pause
	\end{itemize}
	\vspace{15pt}
	MOYENS : 
	\begin{itemize}[label={\textbullet}]
		\item CPA~: Chosen Plaintext Attack \pause
		\item CCA~: Chosen Ciphertext Attack 
	\end{itemize}
\end{frame}


\begin{frame}{NTRU+}{SOTP et Inv}
	\begin{itemize}[label={}]
		\item $\sotp: \{0,1\}^{n} \times \{0,1\}^{2n} \to \{-1,0,1\}^{n}$ \pause
		\item $\qquad \qquad \qquad (x, u) \mapsto y=(x \oplus u_1) - u_2$
		\item $\text{où } u=(u_1 , u_2)$
	\end{itemize}
	\pause
	\vspace{20pt}
	\begin{itemize}[label={}]
		\item $\inv: \{-1,0,1\}^n \times \{0,1\}^{2n} \to \{0,1\}^n$\pause
		\item $\qquad \qquad \quad (y, (u_1,u_2)) \mapsto x\equiv(y+u_2) \oplus u_1 \mod 2$
	\end{itemize}
\end{frame}

\begin{frame}{Attaque}{Non-injectivité de $\inv$}
	\begin{align*}
		\inv: \{-1,0,1\}^n \times \{0,1\}^{2n} \to& \{0,1\}^n \\
		(y, (u_1,u_2)) \mapsto& x\equiv(y+u_2) \oplus u_1 \mod 2 \\
		&\text{où } u_1,u_2\in \{0,1\}^n
	\end{align*}
	\pause
	
	Si $\pi_i(y)=-1$ et $\pi_i(u_2)=\pi_i(u_1)=1$, alors~:
	\begin{align*}
	(\pi_i(y)+\pi_i(u_2))\oplus\pi_i(u_1)=0\oplus 1=1\equiv 1\mod 2
	\end{align*}
	\pause
	
	Et si $\pi_i(y')=\pi_i(y)+2=1$, alors~:
	\begin{align*}
	(\pi_i(y')+\pi_i(u_2))\oplus\pi_i(u_1)=2\oplus 1=3\equiv 1\mod 2
	\end{align*}
	\pause

	$$\inv(y,u) = \inv(y+2e_i,u)$$
\end{frame}

\begin{frame}{Attaque}{Quel chiffré $c'$ choisir\,?}
	\begin{align*}
		& M\in\{-1,0,1\}^n \\
		%& \cdot: \text{produit de polynômes à coefficients dans } \mathbb{Z}/q\mathbb{Z} \\
		& \decaps \text{ va calculer } \inv(M',...)
	\end{align*}
	\vspace{10pt}
	\begin{itemize}[label={}]
		\item $\qquad \quad M'\equiv M+2e_i \mod 3$
		\pause
		\item $\Longleftrightarrow\quad c'\cdot f \equiv c\cdot f + 2e_i \mod 3$
		\pause
		\item $\Longleftrightarrow\quad c'\cdot f \equiv c\cdot f + 2e_i \cdot f \mod 3 \quad (f \equiv 3f'+1 \equiv 1 \mod 3)$
		\pause
		\item $\Longleftrightarrow\quad c'\cdot f \equiv (c + 2e_i)\cdot f \mod 3$
		\pause
		\item $\Longleftrightarrow\quad c' \equiv c + 2e_i \mod 3$
		\pause
		\item $\Longleftarrow\quad c' = c + 2e_i$
	\end{itemize}
\end{frame}

\begin{frame}{Attaque}{A-t-on modifié $r$\,?}
	On a~:
	\begin{align*}
		& c' = c + 2e_i \quad\text{ et }\quad M'\equiv M+2e_i \mod 3 \\
	\end{align*}
	Donc si $\pi_i(M) = -1$~:
	\begin{itemize}[label={}]
		\item \hspace{68pt} $\pi_i(M) = -1$
		\pause
		\item \hspace{35pt} $\Longrightarrow\quad  M' = M + 2e_i \in \{-1,0,1\}^n$
		\pause
		\item \hspace{35pt} $\Longrightarrow\quad  c'-M' = c+2e_i-M-2e_i$
		\pause
		\item \hspace{35pt} $\Longrightarrow\quad  c'-M' = c-M$
		\pause
		\item \hspace{35pt} $\Longrightarrow\quad  r=(c-M)\cdot h^{-1}=(c'-M')\cdot h^{-1}$
	\end{itemize}
	On a retrouvé le même $r$\,!
	\begin{align*}
		m=\inv(M',G(r))=\inv(M,G(r))
	\end{align*}
\end{frame}

\begin{frame}{Attaque}{Probabilités}
	$$M=\sotp(m,(u_1,u_2))=(m\oplus u_1)-u_2$$
	\begin{center}
		\begin{tabular}{cc|c|c|c|c|}
			\cline{3-6}
			& & \multicolumn{4}{c|}{$(u_1,u_2)$} \\
			\cline{3-6}
			& & $(0,0)$ & $(0,1)$ & $(1,0)$ & $(1,1)$ \\
			\cline{1-6}
			\multicolumn{1}{|c}{\multirow{2}{*}{$m$}} & \multicolumn{1}{|c|}{$0$} & $0$ & $-1$ & $1$ & $0$ \\
			\cline{2-6}
			\multicolumn{1}{|c}{} & \multicolumn{1}{|c|}{$1$} & $1$ & $0$ & $0$ & $-1$ \\
			\cline{1-6}
		\end{tabular}
	\end{center}
	
	\begin{align*}
		&(u_1,u_2)=G(r) \quad \text{ uniforme dans } \{0,1\}^{2n} \\
		\Longrightarrow\quad & \mathbb{P}(\pi_i(M)=-1)=\frac{2}{8}=\frac{1}{4}
	\end{align*}

	En essayant tous les $i$ possibles, $\mathbb{P}(\text{échec})=\left(1-\frac{1}{4}\right)^n\approx 0$
\end{frame}

\begin{frame}{Attaque}{Correctif}
	\begin{align*}
		\inv: \{-1,0,1\}^n \times \{0,1\}^{2n} \to& \{0,1\}^n \\
		(y, (u_1,u_2)) \mapsto& x\equiv(y+u_2) \oplus u_1 \mod 2 \\
	\end{align*}
	\begin{align*}
	(\pi_i(y')+\pi_i(u_2))\oplus\pi_i(u_1)=2\oplus 1=3\equiv 1\mod 2
	\end{align*}
	\pause
	$$\downarrow$$
	
	\begin{align*}
		\inv:& \{-1,0,1\}^n \times \{0,1\}^{2n} \to \{0,1\}^n \cup \{\bot\} \\
		&(y, (u_1,u_2)) \mapsto \begin{cases}
			x &\text{ si } (y+u_2) \oplus u_1 \in \{0,1\}^n \\
			\bot &\text{ sinon}
		\end{cases}\\
	\end{align*}
\end{frame}

\begin{frame}[fragile]{Implémentation de référence}{Extrait de l'historique Git}
	\begin{lstlisting}
2d2845a (Update README.md, 2023-08-09)
7fdd38c (commit, 2023-08-09)
c9a78b1 (commit, 2023-08-09)
625524c (commit, 2023-08-08)
5f20f54 (commit, 2023-08-08)
6127e0c (commit, 2023-08-08)
b57581f (commit, 2023-08-08)
5b2aae6 (commit, 2023-08-08)
9fb8b58 (commit, 2023-08-07)
6451ac4 (commit, 2023-08-07)
df07feb (commit, 2023-08-07)
23aa230 (Update ntt.c, 2023-08-04)
19ef8be (Update ntt.c, 2023-08-04)
78693d9 (Update ntt.c, 2023-08-04)
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Implémentation de référence}{Extrait de code}
	\begin{minted}[
		fontsize=\tiny
	]{c}
int16_t fqred16(int16_t a) {
	int16_t t;
	t = a & 0xFFF;
	a >>= 12;
	t +=  (a << 10) - (a << 8)  - (a << 7) - a;
	return t;
}
int16_t fqcsubq(int16_t a) {
	a += (a >> 15) & NTRUPLUS_Q;
	a -= NTRUPLUS_Q;
	a += (a >> 15) & NTRUPLUS_Q;
	return a;
}
void poly_reduce(poly *a) {
	for(int i = 0; i < NTRUPLUS_N; i++) a->coeffs[i] = fqred16(a->coeffs[i]);
}
void poly_freeze(poly *a) {
	poly_reduce(a);
	for(int i = 0; i < NTRUPLUS_N; i++) a->coeffs[i] = fqcsubq(a->coeffs[i]);
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Implémentation de référence}{Extrait de code~: la fonction $\inv$}
	\begin{minted}[
		fontsize=\tiny
	]{c}
int poly_sotp_inv(uint8_t *msg, const poly *a, const uint8_t *buf) {
	uint32_t t1, t2, t3, t4;
	uint32_t r = 0;
	for(int i = 0; i < 2; i++) {
		for(int j = 0; j < 8; j++) {
			t1 = load32_littleendian(buf + 32*i + 4*j);
			t2 = load32_littleendian(buf + 32*i + 4*j + NTRUPLUS_N/8);
			t3 = 0;

			for (int k = 0; k < 2; k++) {
				for(int l = 0; l < 16; l++) {
					t4 = t2 & 0x1;
					t4 = a->coeffs[256*i + 16*l + 2*j + k] + t4;
					r |= t4;
					t4 = (t4^t1) & 0x1;
					t3 ^= t4 << (l+16*k);

					t1 >>= 1;
					t2 >>= 1;
				}
			}
			msg[32*i + 4*j   ] = t3;
			msg[32*i + 4*j + 1] = t3 >> 8;
			msg[32*i + 4*j + 2] = t3 >> 16;
			msg[32*i + 4*j + 3] = t3 >> 24;
		}
	}
	/* ... */
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Implémentation Python}{Démonstration}
	\begin{minted}{python}
n = 5
q = 53

### pk, sk = keygen(n, q)
pk = [33, 40, 49, 5, 43]
sk = [51, 0, 3, 3, 3]

### K1, c = encaps(n, q, pk)
c  = [32, 36, 3, 2, 43]
K1 = [1, 0, 1, 1, 0]

### K2 = decaps(n, q, pk, sk, c)
K2 = [1, 0, 1, 1, 0]

### K3 = decaps(n, q, pk, sk, c + 2X)
K3 = [1, 0, 1, 1, 0]
\end{minted}
\end{frame}

\begin{frame}[fragile]{Implémentation Python}{Démonstration (probabilité de réussite)}
Avec $c'=c+2$~:
	\begin{minted}{python}
n = 576
q = 3457

wins= 260  fails= 740
Attack win rate: 26.0 %
\end{minted}
\end{frame}

\begin{frame}[fragile]{Implémentation Python}{Démonstration (probabilité de réussite)}
Avec $c'=c+2e_i$ pour chaque $i$ jusqu'à réussite~:
	\begin{minted}{python}
n = 576
q = 3457

wins= 1000  fails= 0
Attack win rate: 100.0 %
\end{minted}
\end{frame}

\begin{frame}{Bibliography}
	\begin{itemize}
		\item Implémentation de référence et spécification NTRU+
		\item \textit{A Novel CCA Attack for NTRU+ KEM}, 2023, Lee et al
		\item \textit{NTRU Algorithm Specifications And Supporting Documentation}, 2019, Chen et al
		\item \textit{NTRU+: Compact Construction of NTRU Using Simple Encoding Method}, 2022, Kim \& Park
		\item \textit{NTRU: A ring-based public key cryptosystem}, 1998, Hoffstein \& Pipher \& Silverman
		\item \textit{A Thorough Treatment of Highly-Efficient NTRU Instantiations}, 2021, Duman et al
		\item \textit{NTTRU: Truly Fast NTRU Using NTT}, 2019, Lyubashevsky \& Seiler
	\end{itemize}
\end{frame}

% \begin{frame}{Attacks}{Malleability}
% 	CBC does not ensure integrity nor authenticity!\\
% 	If you know a block's plaintext, you can control subsequent blocks.
% 	\begin{figure}[H]
% 		\centering
% 		\includegraphics[width=10cm]{cbc.png}
% 	\end{figure}
% \end{frame}

% \begin{frame}{Plausible denialability}{Pretend there is something else (steganography)}
% 	Yes, I have a hard disk full of random pictures, and so what?
% 	\center
% 	\begin{figure}[H]
% 		\centering
% 		\begin{subfigure}[b]{0.25\textwidth}
% 			\includegraphics[width=\textwidth]{stegano_tree.png}
% 			\caption{Original}
% 		\end{subfigure}
% 		~
% 		\begin{subfigure}[b]{0.25\textwidth}
% 			\includegraphics[width=\textwidth]{stegano_cat.png}
% 			\caption{Extracted}
% 		\end{subfigure}
% 	\end{figure}
% \end{frame}

\end{document}
