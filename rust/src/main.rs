pub mod poly;

use num_traits::Num;
use poly::Poly;
use std::ops::Neg;

fn main() {
	let sets = NtruSettings::<i32> {
		n: 107,
		p: 3,
		q: 64,
		df: 15,
		dg: 12,
		d: 5,
	};
	let ntru = Ntru::new(sets);
	let mut m = Poly(vec![0; 107]);

	let iters: usize = 100;
	let mut successes: usize = 0;
	for _ in 0..iters {
		let KeyPair { pk, sk } = ntru.generate_keys();
		m.set_random_bin();
		let c = ntru.encrypt(&pk, m.clone());
		let m2 = ntru.decrypt(&sk, c);
		if let Some(m2) = m2 {
			if m2 == m {
				successes += 1;
			} else {
				// println!("{:?}", m2 - &m);
			}
		}
	}
	println!("Success rate: {}", successes as f32 / iters as f32);
}

/// Public Key CryptoSystem
pub trait PKCS<Settings, PK, SK, M, C> {
	fn new(settings: Settings) -> Self;
	fn generate_keys(&self) -> KeyPair<PK, SK>;
	fn encrypt(&self, pk: &PK, m: M) -> C;
	fn decrypt(&self, sk: &SK, c: C) -> Option<M>;
}

pub struct KeyPair<PK, SK> {
	pk: PK,
	sk: SK,
}

pub struct NtruSettings<T> {
	pub n: usize,
	pub p: T,
	pub q: T,
	pub df: usize,
	pub dg: usize,
	pub d: usize,
}

pub struct Ntru<T> {
	settings: NtruSettings<T>,
}

impl<T: Copy + Num + Neg<Output = T> + std::iter::Sum<T> + Ord>
	PKCS<NtruSettings<T>, Poly<T>, Poly<T>, Poly<T>, Poly<T>> for Ntru<T>
{
	fn new(settings: NtruSettings<T>) -> Self {
		Ntru { settings }
	}

	fn generate_keys(&self) -> KeyPair<Poly<T>, Poly<T>> {
		let f = Poly::<T>::random_pm0(
			self.settings.df,
			self.settings.df - 1,
			self.settings.n - 2 * self.settings.df + 1,
		);
		let g = Poly::<T>::random_pm0(
			self.settings.dg,
			self.settings.dg,
			self.settings.n - 2 * self.settings.dg,
		);
		// let fp: Poly<T> = f.clone() * self.settings.p + T::one();
		let fq: Poly<T> = f.clone() * self.settings.q + T::one();
		KeyPair {
			pk: g.circ_conv(&fq, self.settings.q),
			sk: f,
		}
	}

	fn encrypt(&self, pk: &Poly<T>, m: Poly<T>) -> Poly<T> {
		let phi = Poly::<T>::random_pm0(
			self.settings.d,
			self.settings.d,
			self.settings.n - 2 * self.settings.d,
		);
		(phi.circ_conv(pk, self.settings.q) * self.settings.p + m) % self.settings.q
	}

	fn decrypt(&self, sk: &Poly<T>, c: Poly<T>) -> Option<Poly<T>> {
		let a = c.circ_conv(sk, self.settings.q).center(self.settings.q);
		let fp: Poly<T> = sk.clone() * self.settings.p + T::one();
		Some(fp.circ_conv(&a, self.settings.p))
	}
}
