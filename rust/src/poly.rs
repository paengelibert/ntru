use num_traits::{One, Zero};
use rand::Rng;
use std::{
	iter::Sum,
	ops::{Add, Div, Mul, Neg, Rem, Sub},
};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Poly<T>(pub Vec<T>);

impl<T: Copy + Add<T, Output = T>> Add<Poly<T>> for Poly<T> {
	type Output = Poly<T>;
	fn add(mut self, rhs: Poly<T>) -> Self::Output {
		assert_eq!(self.0.len(), rhs.0.len());
		self.0
			.iter_mut()
			.zip(rhs.0.iter())
			.for_each(|(a, b)| *a = *a + *b);
		self
	}
}
impl<T: Copy + Add<T, Output = T>> Add<&Poly<T>> for Poly<T> {
	type Output = Poly<T>;
	fn add(mut self, rhs: &Poly<T>) -> Self::Output {
		assert_eq!(self.0.len(), rhs.0.len());
		self.0
			.iter_mut()
			.zip(rhs.0.iter())
			.for_each(|(a, b)| *a = *a + *b);
		self
	}
}
impl<T: Copy + Add<T, Output = T>> Add<T> for Poly<T> {
	type Output = Poly<T>;
	fn add(mut self, rhs: T) -> Self::Output {
		let x = self
			.0
			.first_mut()
			.expect("Cannot add scalar to empty polynomial!");
		*x = *x + rhs;
		self
	}
}
impl<T: Copy + Sub<T, Output = T>> Sub<&Poly<T>> for Poly<T> {
	type Output = Poly<T>;
	fn sub(mut self, rhs: &Poly<T>) -> Self::Output {
		assert_eq!(self.0.len(), rhs.0.len());
		self.0
			.iter_mut()
			.zip(rhs.0.iter())
			.for_each(|(a, b)| *a = *a - *b);
		self
	}
}
impl<T: Copy + Rem<T, Output = T>> Rem<T> for Poly<T> {
	type Output = Poly<T>;
	fn rem(mut self, rhs: T) -> Self::Output {
		self.0.iter_mut().for_each(|a| *a = *a % rhs);
		self
	}
}
impl<T: Copy + Sum<T> + Mul<T, Output = T>> Mul<Poly<T>> for Poly<T> {
	type Output = Poly<T>;
	fn mul(mut self, rhs: Poly<T>) -> Self::Output {
		assert_eq!(self.0.len(), rhs.0.len());
		for k in 0..self.0.len() {
			let (g1, g2) = rhs.0.split_at(k);
			self.0[k] = self
				.0
				.iter()
				.zip(g1.iter().rev().chain(g2.iter().rev()))
				.map(|(a, b)| *a * *b)
				.sum();
		}
		self
	}
}
impl<T: Copy + Sum<T> + Mul<T, Output = T>> Mul<&Poly<T>> for Poly<T> {
	type Output = Poly<T>;
	fn mul(mut self, rhs: &Poly<T>) -> Self::Output {
		assert_eq!(self.0.len(), rhs.0.len());
		for k in 0..self.0.len() {
			let (g1, g2) = rhs.0.split_at(k);
			self.0[k] = self
				.0
				.iter()
				.zip(g1.iter().rev().chain(g2.iter().rev()))
				.map(|(a, b)| *a * *b)
				.sum();
		}
		self
	}
}
impl<T: Copy + Mul<T, Output = T>> Mul<T> for Poly<T> {
	type Output = Poly<T>;
	fn mul(mut self, rhs: T) -> Self::Output {
		self.0.iter_mut().for_each(|x| *x = *x * rhs);
		self
	}
}

impl<T> Poly<T> {
	pub fn center(mut self, q: T) -> Self
	where
		T: Copy + Sub<T, Output = T> + Add<T, Output = T> + Ord + Div<T, Output = T> + One,
	{
		let q2 = q / (T::one() + T::one());
		self.0.iter_mut().for_each(|a| {
			if *a > q2 {
				*a = *a - q
			}
		});
		self
	}

	/// Samples random polynomial of degree dp+dm+d0
	/// with dp 1's, dm -1's, d0 0's.
	pub fn random_pm0(dp: usize, dm: usize, d0: usize) -> Self
	where
		T: Copy + One + Neg<Output = T> + Zero,
	{
		let mut rng = rand::thread_rng();
		let mut elems: Vec<T> = (0..dp)
			.map(|_| T::one())
			.chain((0..dm).map(|_| T::one().neg()))
			.chain((0..d0).map(|_| T::zero()))
			.collect();
		for i in 0..elems.len() {
			let k = rng.gen_range(i..elems.len());
			elems.swap(i, k);
		}
		Self(elems)
	}

	/// Assigns random bits to coefficients
	pub fn set_random_bin(&mut self)
	where
		T: One + Zero + Copy,
	{
		let mut rng = rand::thread_rng();
		let elems = [T::zero(), T::one()];
		self.0
			.iter_mut()
			.for_each(|x| *x = elems[rng.gen_range(0..2)]);
	}

	// pub fn inv(self, modulus: T) -> Option<Self>
	// where
	// 	T: Copy,
	// {
	// 	let mut r = Self(vec![T::zero(); self.deg()]);
	// }

	pub fn deg(&self) -> usize {
		self.0.len()
	}

	/// Modular circular convolution product, as described in NTRU98
	pub fn circ_conv(mut self, rhs: &Poly<T>, modulus: T) -> Poly<T>
	where
		T: Mul<T, Output = T> + Add<T, Output = T> + Copy + Rem<T, Output = T> + Zero,
	{
		assert_eq!(self.0.len(), rhs.0.len());
		for k in 0..self.0.len() {
			let (g1, g2) = rhs.0.split_at(k);
			self.0[k] = self
				.0
				.iter()
				.zip(g1.iter().rev().chain(g2.iter().rev()))
				.fold(T::zero(), |s, (a, b)| (s + *a * *b) % modulus);
		}
		self
	}
}
