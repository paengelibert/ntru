\documentclass[french,11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
%\usepackage{authblk}
\usepackage{enumitem}
\usepackage[top=2cm,bottom=2cm]{geometry}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{caption}
\usepackage{subcaption}
%\usepackage{tikz}
\usepackage{csquotes}
\usepackage{minted}
\usepackage{xcolor}
\usepackage[
  backend=biber,
  style=numeric,
  %sorting=ynt
]{biblatex}
\usepackage{mathtools, stmaryrd}
\usepackage{multirow}

\author{Pascal Engélibert, Paul Capgrand}
\title{Une attaque CCA de NTRU$+$}
%\affil{Université de Bordeaux}
%\date{un jour en 2024}

\usepackage{titlesec}

%\usetikzlibrary{math}
%\usetikzlibrary{fixedpointarithmetic}

%\titleformat{\chapter}{\normalfont\huge}{\thechapter.}{22pt}{\huge}

\addbibresource{report.bib}
% compile process: pdflatex report.tex -> biber report -> pdflatex.tex -> pdflatex.tex

\DeclareMathOperator{\ppcm}{ppcm}
\DeclareMathOperator{\pgcd}{pgcd}
\DeclareMathOperator{\rem}{rem}
\DeclareMathOperator{\sotp}{SOTP}
\DeclareMathOperator{\inv}{Inv}
\DeclareMathOperator{\keygen}{KeyGen}
\DeclareMathOperator{\encaps}{Encaps}
\DeclareMathOperator{\decaps}{Decaps}

\DeclarePairedDelimiterX{\Iintv}[1]{\llbracket}{\rrbracket}{\iintvargs{#1}}
\NewDocumentCommand{\iintvargs}{>{\SplitArgument{1}{,}}m}
{\iintvargsaux#1} %
\NewDocumentCommand{\iintvargsaux}{mm} {#1\mkern1.5mu..\mkern1.5mu#2}

\begin{document}
\maketitle
\tableofcontents
\pagebreak

\section{Introduction}

Nous étudions un article publié dans le contexte de la compétition KpqC (Korean Post-Quantum Cryptography) débutée en 2022. Comme son nom l'indique, cette compétition a pour but de standardiser des cryptosystèmes résistants à l'ordinateur quantique en Corée du Sud.
L'article en question, rédigé par les coréens Joohee Lee, Minju Lee, Hansol Ryu et Jaehui Park\cite{lee23}, présente une attaque sur un des mécanismes d'encapsulation de clé (KEM) à base de réseau euclidien proposé dans cette compétition~: NTRU$+$\cite{cryptoeprint:2022/1664}.
Ce KEM est toujours en lice aujourd'hui et la compétition en est rendue à son deuxième round qui devrait se conclure aux alentours de novembre de cette année.\\
Nous proposons dans le cadre de ce rapport, d'étudier ce mécanisme ainsi que l'attaque à laquelle il est vulnérable. Pour cela, nous définirons ce qu'est NTRU, ce que sont les KEM pour comprendre comment est construit NTRU$+$. Nous regarderons rapidement ensuite les problèmes sur lesquels il est basé et quel est le niveau de sécurité auquel il peut prétendre.
Nous présenterons l'attaque telle qu'elle est décrite dans l'article de J.Lee\cite{lee23} et ce pourquoi elle est réalisable. Puis nous parlerons de l'implémentation de cette attaque que nous avons mise au point en Python. Nous finirons par nous intéresser à la solution proposée pour corriger cette faille et ce qu'elle implique sur la sécurité annoncée de NTRU$+$.
\\

\section{NTRU+}

Comme nous l'avons présenté en introduction, NTRU$+$ est un KEM pour Key Encapsulation Mechanism.
C'est à dire qu'il est utilisé pour l'échange de clé secrète. On utilise donc un système à clé publique, asymétrique, pour mettre en place un système à clé secrète symétrique.
C'est ce que l'on appelle de la cryptographie hybride.

NTRU$+$, ou les KEM plus généralement, génèrent eux-même la clé de session qu'ils encapsulent. Il ne s'agit pas de les utiliser pour transmettre une clé que l'on aurait choisi.

Avant de présenter le fonctionnement du schéma NTRU$+$, nous allons d'abord revenir sur le fonctionnement de NTRU, qui n'est pas un KEM à la base, à partir duquel il est construit.

\subsection{NTRU}

NTRU est un système de cryptographie asymétrique, aussi appelé PKE pour Public Key Encryption scheme, reposant sur des hypothèses des réseaux euclidiens.
Il est développé en 1996 par les mathématiciens Jeffrey Hoffstein, Jill Pipher et Joseph H. Silverman\cite{10.1007/BFb0054868}. Cela en fait un cryptosystème relativement récent à l'échelle de la cryptographie mais aussi un des plus vieux parmis ceux à base de réseaux.
Les opérations sont effectuées dans un anneau de polynômes tronqués $\mathcal{R} = \mathbb{Z}[X] / (X^N - 1)$ d'où son nom (N-th degree TRUncated polynomial) muni du produit de convolution.

L'anneau $\mathcal{R}$ peut même être défini de manière plus générale comme $\mathbb{Z}[X] / \langle f(X) \rangle$.
Dans un premier temps, nous gardons la première définition de cet anneau de polynômes pour définir comment fonctionne NTRU.

\subsubsection{Définition de NTRU}

Il existe plusieurs façon de paramétrer NTRU. Voici une version simplifiée de HPS c'est à dire celle de Hoffstein, Pipher et Silverman.

\paragraph{Génération des clés}

\begin{itemize}[label={\textbullet}]
	\item Choix des paramètres~: $ N, p, q, t $.\\
		$N$ est le degré maximal des polynômes, $p$ est un petit modulo (usuellement $p=3$), $q$ premier avec $p$ et considérablement plus grand. Et enfin $t$, qui est bien plus petit que $N$, pour la distribution des coefficients des polynômes suivants.
	\item Générer deux polynômes $\mathbf{f},\mathbf{g}\in \mathcal{R}$ à coefficients dans $\{-1,0,1\}$.\\
		$\mathbf{g}$ doit avoir $t$ coefficients valant $1$ et $t$ coefficients valant $-1$.\\
		$\mathbf{f}$ doit être inversible modulo $p$ et modulo $q$ et on note $\mathbf{f}_p$ et $\mathbf{f}_q$ ces deux inverses. $\mathbf{f}$ a aussi $t$ coefficients valant $1$ mais $t-1$ valant $-1$.
	\item La clé publique est $\mathbf{h} = p\mathbf{f}_q \cdot \mathbf{g}\mod q$.
	\item La clé privée est $(\mathbf{f}, \mathbf{f}_p)$.
\end{itemize}

\paragraph{Chiffrement}

\begin{itemize}[label={\textbullet}]
	\item Le message $\mathbf{m}$ est un polynôme à coefficients dans $\left[-\frac{p-1}{2},\frac{p-1}{2}\right]$ avec $p$ impair, de degré inférieur à $N$. Usuellement, on a un message en ternaire (voire binaire) puisque $p = 3$
	\item Choix d'un polynôme $\mathbf{r}$ à coefficients dans $\{-1,0,1\}$ avec presque tous ces coefficients nuls (à la manière de $\mathbf{f}$ et $\mathbf{g}$).
	\item Calcul du chiffré $\mathbf{e}$ : \[ \mathbf{e} = \mathbf{r} \cdot \mathbf{h} + \mathbf{m} \mod q \]
\end{itemize}

\paragraph{Déchiffrement}

\begin{itemize}[label={\textbullet}]
	\item Calcul de $\mathbf{a} = \mathbf{f} \cdot \mathbf{e} \mod q$
	\item Calcul de $\mathbf{b} = \mathbf{a} \mod p$
	\item On retrouve $\mathbf{m}$ : \[ \mathbf{m} = \mathbf{b} \cdot \mathbf{f}_p \mod p \]
\end{itemize}

\vspace{12pt}
\paragraph{}
Nous remarquons que NTRU n'utilise que des opérations simples sur des polynômes dont la plupart avec des coefficients triviaux, le plus demandant étant l'inversion modulo $q$ (l'inversion modulo $p$ peut être évitée dans la pratique, nous y reviendrons). 
Remarquons également que $\mathbf{f}$ a $t-1$ coefficients valant $-1$ pour éviter que $\mathbf{f}(1)=0$ ce qui implique $\mathbf{f}$ non-inversible dans $\mathcal{R}$.\\ En effet, si $(X - 1) \mid \mathbf{f}$ alors $\pgcd\left(\mathbf{f},X^{N}-1\right) \neq 1 $.

\paragraph{}
Les polynômes $\mathbf{f}$, $\mathbf{g}$ et $\mathbf{r}$ sont de petites normes (si on les considère comme des vecteurs). Tandis que la clé publique $\mathbf{h}$ et le chiffré $\mathbf{e}$ sont de tailles bien plus importantes.
C'est une caractéristique commune des différentes versions de NTRU.

\subsubsection{Correct}

Vérifions maintenant que ce schéma nous permet effectivement de retrouver $m$ lorsqu'il est chiffré puis déchiffré avec la méthode décrite ci-dessus.
\\Reprenons le chiffré de $\mathbf{m}$~: $\mathbf{e} = \mathbf{r} \cdot \mathbf{h} + \mathbf{m} \mod q$
\\Pour le déchiffrer, nous avons le calcul de $\mathbf{a}$ suivant~:
\begin{align*}
	\mathbf{a} &= \mathbf{f} \cdot \mathbf{e} \mod q \\
	\mathbf{a} &= \mathbf{f} \cdot (\mathbf{r} \cdot \mathbf{h} + \mathbf{m}) \mod q \\
	\mathbf{a} &= \mathbf{f} \cdot (\mathbf{r} \cdot p\mathbf{f}_q \cdot \mathbf{g} + \mathbf{m}) \mod q \\
	\mathbf{a} &= p\mathbf{r} \cdot \mathbf{g} + \mathbf{f} \cdot \mathbf{m} \mod q
\end{align*}

Afin de se débarasser de la partie randomisée, notons-la $\mathbf{g}' := p\mathbf{r} \cdot \mathbf{g}$, nous allons à présent évaluer modulo $p$.
Cependant, rien ne garantit que les coefficients de $\mathbf{g}'$ soient effectivement multiples de $p$ une fois modulo $q$.
Si on écrit la division euclidienne par $q$ d'un coefficient de $\mathbf{g}'$, $\mathbf{g}_i' = qk + r$, $k \in \mathbb{Z}$ et $0<r<q$ alors on a $r = \mathbf{g}_i' - qk$ et donc~:
\begin{align*}
	p \mid r &\iff p \mid \mathbf{g}_i' - qk \\
	&\iff p \mid qk \text{ ($\mathbf{g}_i'$ est multiple de $p$)}\\
	&\iff p \mid k \text{  (car $pgcd(p,q) = 1$)} \\
	&\iff k=0 \text{  ou $k_{>0}$ multiple de $p$}
\end{align*}

Pour un déchiffrement réussi, il faudrait que $k=0$ pour tous les coefficients de $\mathbf{g}'$. C'est-à-dire qu'ils soient naturellement inférieurs à $q$.
C'est pour cette raison que nous voulons $\mathbf{r}$ avec des petits coefficients et $q$ suffisamment grand.


\paragraph{}
Ce qui donne,
\begin{align*}
	\mathbf{b} &= \mathbf{a} \quad (\text{mod  } p) \\
	\mathbf{b} &= \mathbf{f} \cdot \mathbf{m} \quad (\text{mod  } p) \\
	\mathbf{f}_p \cdot \mathbf{b} &= \mathbf{f}_p \cdot \mathbf{f} \cdot \mathbf{m} \quad (\text{mod  } p) \\
	\mathbf{f}_p \cdot \mathbf{b} &= \mathbf{m} \quad (\text{mod  } p)
\end{align*}

On récupère bien le message $\mathbf{m}$ comme polynôme de $\mathcal{R}$ à la condition que les coefficients de $\mathbf{g}'$ ne soient pas trop grands. \\
NTRU est donc bien correct, mais il requiert une certaine distribution des coefficients des polynômes. Si celle-ci n'est pas respectée, le déchiffrement pourrait échouer. Un adversaire qui aurait la possibilité de chiffrer ses messages avec des $\mathbf{m}$ et $\mathbf{r}$ de son choix, sous-entendu en dehors des espaces dans lesquels ils sont normalement choisis, pourrait obtenir de l'information sur les polynômes secrets en utilisant ces erreurs de déchiffrements.

\subsection{KEM et NTRU$+$}
Dans cette section, nous définissons plus rigoureusement ce qu'est un KEM et ce que cela implique dans la construction de NTRU$+$.

\subsubsection{Définition KEM}
Un mécanisme d'encapsulation ou KEM (Key Encapsulation Mechanism) est un triplet d'algorithmes $(\keygen,\encaps,\decaps)$.
\begin{itemize}[label={\textbullet}]
	\item $\keygen$ est l'algorithme de génération de clés. Il prend en entrée le paramètre de sécurité $\lambda$, autrement dit le nombre de bits de sécurité et sort un couple $(pk,sk)$ composé d'une clé publique et de la clé privée associée. Il est probabiliste.
	\item $\encaps$ est l'algorithme d'encapsulation. Il prend en entrée la clé publique $pk$ et sort une clé $K$ qui est la clé secrète partagée ainsi qu'un chiffré $c$ qui sera envoyé au possesseur de la clé privée $sk$. Il est également probabiliste.
	\item $\decaps$ est l'algorithme de décapsulation. Il prend en entrée la clé privée $sk$ et le chiffré $c$ et il sort la clé $K$ si la décapsulation est valide, $\bot$ sinon. Il est déterministe.
\end{itemize}

\paragraph{}
De manière générale, les KEM sont construits à partir de PKE (Public Key Encryption scheme). Ceux qui sont basés sur des problèmes de réseaux sont parmis les plus rapides des KEM post-quantiques pour des tailles de clés publiques et de chiffrés de l'ordre d'1 KB pour un niveau de sécurité correspondant à AES-128. C'est le cas de NTRU$+$.\\
Ils peuvent aussi être résistants aux attaques à chiffrés choisis (plus particulièrement IND-CCA) en applicant certaines transformations si le PKE a de bonnes propriétés. Nous y reviendrons dans la partie 3 sécurité.\\


\subsubsection{Définition NTRU+}
NTRU$+$ utilise $\mathcal{R} = \mathbb{Z}_{3457}[X] / (X^{N} - X^{N/2} + 1)$ avec $q=3457$ premier et $N = 2^{i}3^{j}$. En l'occurrence, ils proposent 4 versions~: \[N = 2^{7}\times3^{2} = \mathbf{1152},\quad 2^{5}\times3^{3} = \mathbf{864},\quad 2^{8}\times3^{1} = \mathbf{768} \text{\: et \:} 2^{6}\times3^{2} = \mathbf{576} \]
Ces anneaux de polynômes permettent l'utilisation de la NTT (Number Theoretic Transform) pour la multiplication rapide de long polynômes. Cela permet d'accéler les trois algorithmes du KEM d'un facteur $10$ par rapport au NTRU-KEM qui avait été proposé au NIST en 2018.
Ce sont Vadim Lyubashevsky et Gregor Seiler qui sont à l'origine de cette idée en 2019. Ils ont astucieusement nommé leur version NTTRU\cite{Lyubashevsky_Seiler_2019}.

\paragraph{}
Les trois algorithmes utilisés par NTRU$+$ dans sa version IND-CCA sont construits en utilisant une transformation de Fujisaki-Okamoto pour les KEM et un algortihme appelé $\sotp$ que nous expliquerons dans la sous-section suivante.
Voyons maintenant comment sont définis les algorithmes de NTRU$+$~: $\keygen$, $\encaps$ et $\decaps$.

\paragraph{KeyGen}
$(1^{\lambda})\rightarrow(pk,sk)$
\begin{itemize}[label={\textbullet}]
	\item Générer $f'$ et $g$ à coefficients dans $\{-1,0,1\}$ à partir d'une distribution binomiale centrée sur $\mathbb{Z}$.
	\item $f = 3f' + 1$ C'est ainsi que l'on peut s'économiser le calcul de l'invers de $f_{3}$ puisque $f = 1$ (mod 3). Cela réduit la clé privée à $f$ seulement.
	\item Si les inverses de f et g modulo $q$ n'existent pas, recommencer.
	\item $(pk,sk) = (h = 3g \cdot f^{-1} \text{mod } q,f)$ Comme vu dans NTRU, à la différence que l'on veut aussi pouvoir inverser $g$, et donc $h$, modulo $q$. 
\end{itemize}

\paragraph{Encaps}
$(pk)\rightarrow(K,c)$
\begin{itemize}[label={\textbullet}]
	\item Générer $m$ un n-uplet à valeur dans $\{0,1\}$ qui sert de message pour la fonction de hachage cryptographique $H : \{0,1\}^{*} \rightarrow \{0,1\}^{n} \times \mathcal{K}$
	\item $(r,K) = H(m)$
	\item $M = \text{SOTP}(m,G(r))$ avec $G : \{0,1\}^{*} \rightarrow \{0,1\}^{2n}$ une autre fonction de hash et SOTP qui est la nouveauté qu'apporte NTRU$+$ par rapport à ses prédécesseurs.
	\item $c = h \cdot r + M$ (mod $q$) M est chiffré avec NTRU.
\end{itemize}

\paragraph{Decaps}
$(sk,c)\rightarrow K \text{ ou } \bot$
\begin{itemize}[label={\textbullet}]
	\item $M = (c \cdot f \mod q) \mod 3$ Déchiffrement de NTRU qui permet de retrouver $M$.
	\item $r = (c - M) \cdot h^{-1} \mod q$ Contrairement à NTRU seul, on veut aussi récupérer le polynôme $r$. C'est pourquoi il faut aussi l'inverse de $h \mod q$. On dit que cette version du PKE, appelée GenNTRU\cite{cryptoeprint:2021/1352}, est RR (randomness-recoverable).
	\item $m = \inv(M,G(r))$ ($\inv$ étant la réciproque de $\sotp$)
	\item $(r',K) = H(m)$
	\item si $r = r'$ retourner $K$, sinon retourner $\bot$.
\end{itemize}


\subsubsection{SOTP}
Intéressons nous maintenant à l'algorithme d'encodage SOTP pour Semi-generalized One Time Pad et à l'algorithme réciproque de décodage Inv apportés par Jonghyun Kim et Jong Hwan Park dans NTRU$+$.

SOTP est un dérivé du GOTP pour Generalized One Time Pad introduit en 2021 par Julien Duman et al. \cite{cryptoeprint:2021/1352} Il s'agit d'un codage utilisant un One Time Pad au niveau de l'encapsulation qui permet au KEM d'avoir un taux d'erreur négligeable dans le pire cas sans ajouter de redondance aux chiffrés.
Cela permet de réduire la taille des paramètres par rapport aux versions de Lyubashevsky pour le même niveau de sécurité.
Cependant GOTP demande une distribution uniforme de $m$ sur l'espace $\{-1,0,1\}^{n}$ ce qui est difficile à implémenter en temps constant avec la méthode de rejet.

SOTP apparaît comme une amélioration de GOTP en enlevant cette contrainte de distribution pour $m$ et permet une distribution arbitraire sur $\{0,1\}^{n}$.

Voici comment sont définis SOTP et Inv avec $\oplus$ le XOR bit à bit~:

\begin{align*}
	\sotp: \{0,1\}^{n} \times \{0,1\}^{2n} \to& \{-1,0,1\}^{n} \\
	(x, u) \mapsto& y=(x \oplus u_1) - u_2 \\
	&\text{où } u=(u_1 , u_2)
\end{align*}

\begin{align*}
	\inv: \{-1,0,1\}^n \times \{0,1\}^{2n} \to& \{0,1\}^n \\
	(y, u) \mapsto& x\equiv(y+u_2) \oplus u_1 \mod 2 \\
	&\text{où } u=(u_1 , u_2)
\end{align*}

\paragraph{}
Il est facile de vérifier que Inv($\sotp(m,u$),$u$) $ = m$. En revanche, si $y$ est altéré, $(y + u_2)$ pourrait ne pas être binaire et l'opération $\oplus u_1$ mal définie.\\
Nous y reviendrons dans la section 4 puisque c'est sur l'ambiguïté dans la définition de cette opération que se joue l'attaque.


\subsubsection{Correct}
Pour qu'un KEM soit correct, il faut que les clés retournées par Encaps et Decaps soient les mêmes. Cependand, les KEM ne sont pas tous parfaitement corrects. Chez certain, il existe une faible probabilité d'échec où ces valeurs sont différentes.

On définit l'erreur de correction à l'aide d'un jeu~:

\begin{center}
	\begin{tabular}{|l|}
	\hline
	\multicolumn{1}{|c|}{$G_{\text{KEM}}^{\text{correct}}$} \\
	\hline
	\\
	$(sk,pk) \leftarrow \keygen()$ \\
	$(k,c) \leftarrow \encaps(pk)$ \\
	\textbf{if} $\bot(k)$ \textbf{return} $0$ \\
	$k' \leftarrow \decaps(sk, c)$ \\
	\bf{return} $k \neq k'$ \\
	\hline
	\end{tabular}
\end{center}

La fonction $\perp$ retourne $1$ si $k$ n'est pas une clé valide. Cela peut arriver avec certains algorithmes.\\
Ce jeu simule donc une instance du KEM et retourne $1$ si et seulement si les clés obtenues par Encaps et Decaps sont différentes. 
L'erreur de correction (correctness error) est alors définie par~:
\[\mathbf{Cor}_{KEM}=Pr[G_{\text{KEM}}^{\text{correct}} = 1]\]

Plus $\mathbf{Cor}_{KEM}$ est proche de $0$ et plus le KEM est correct.

\section{Sécurité}
L'attaque que nous allons étudier dans la prochaine section casse la sécurité OW-CCA de NTRU$+$, et donc, nous allons le voir, également la sécurité IND-CCA que NTRU$+$ est supposé avoir.
\subsection{OW-CCA}
Ces acronymes séparés par un tiret représentent le but et les moyens d'un attaquant. Ici, c'est le sens-unique (One Wayness) en attaque à chiffrés choisis adaptative (Chosen Ciphertext Attack) qui est remis en cause.
C'est à dire que si l'attaquant a accès à un oracle de déchiffrement, qui peut déchiffrer n'importe quel chiffré sauf le challenge (CCA), il est capable de retrouver le message caché derrière ce challenge (OW). En IND-CCA, le but de l'attaquant est de réussir à distinguer quel message a été chiffré parmi les deux qu'il a soumis (INDistinguishability) avec encore l'aide d'un oracle de déchiffrement (les moyens du CCA).
C'est un niveau de sécurité plus difficile à atteindre puisque l'on veut encore plus restreindre l'objectif que pourrait viser l'attaquant. Et c'est aussi pour cela que si le OW-CCA est brisé alors le IND-CCA l'est aussi.\\
Regardons rapidement pourquoi NTRU$+$ est pourtant bien censé être de sécurité IND-CCA si l'on en croit leurs démonstrations~: Le PKE GenTRU utilisé est de sécurité OW-CPA (One-Wayness en Chosen Plaintext Attack), il est aussi RR (randomness recoverable) et enfin MR (message recoverable). Grâce à ces propriétés, l'algorithme de SOTP permet non seulement de réduire le taux d'erreur, passage de average-case à worst-case qu'ils notent $ACWC_{2}$, mais aussi d'augmenter d'un cran la sécurité en passant à IND-CPA.
La version de la transformation de Fujisaki-Okamoto (FO$^{\perp}$) pour les KEM qu'ils utilisent permet le passage de IND-CPA à IND-CCA. Cette transformation correspond à l'utilisation de la fonction de hachage $H : \{0,1\}^{*} \rightarrow \{0,1\}^{n} \times \mathcal{K}$ dans Encaps pour obtenir le couple $(r,K)$ à partir de $m$ puis dans Decaps à partir de $m'$ pour obtenir $(r',K')$ afin de comparer $r$ et $r'$ ce qui permet de savoir si le $M$ déchiffré en premier lieu est bien légitime ou si il a pu être manipulé par un attaquant.\\

\[
\underset{\text{OW-CPA}}{\text{GenTRU}} \overset{\text{ACWC$_{2}$}}{\longrightarrow } \underset{\text{IND-CPA}}{\text{CPA-NTRU}+} \overset{\text{FO}^{\perp}}{\longrightarrow } \underset{\text{IND-CCA}}{\text{CCA-NTRU}+}
\]
\\

\subsection{Problème NTRU, RingLWE et réseaux}

La sécurité de NTRU$+$ repose sur des problèmes que l'on pense difficile, notamment le problème NTRU. Donnons une variante recherche de ce problème simple à énoncer~: Pour une instance $h$ de NTRU (comprendre ici que $h$ est la clé publique) il s'agit de trouver la trappe $(f,g)$ avec $f$ et $g$ les plus petits possibles. C'est à dire que l'on doit réussir à trouver un couple, qui permettrait de casser le "sens-unique" du chiffrement, suffisamment petits pour que le déchiffrement soit correct (cf l'importance de la taille des coefficients dans la partie 2.1.2).

La sécurité de NTRU$+$ repose aussi sur le problème RingLWE (Learning With Errors sur les anneaux) que l'on suppose difficile. Celui-ci peut être énoncé de deux manières~: en tant que problème de décision ou en tant que problème de recherche.\\
Soient~:
\begin{itemize}[label={\textbullet}]
	\item $h_i(x)$ un ensemble de polynômes aléatoires \textbf{connus} d'un anneau $F_q[x]/\Phi(x)$ à coefficients dans $F_q$. Reprenons les mêmes notations que pour NTRU$+$ avec $\mathcal{R}$ ie $F_q = \mathbb{Z}_q$ et $\Phi(x)= x^{N} - x^{N/2} + 1$
	\item $m_i(x)$ un ensemble de petits polynômes aléatoires \textbf{inconnus} du même anneau $\mathcal{R}$
	\item $r(x)$ un petit polynôme \textbf{inconnu} de $\mathcal{R}$
	\item $c_i(x) = (h_i(x) \cdot r(x)) + m_i(x)$
\end{itemize}

Dans sa version recherche, il s'agit de trouver le polynôme inconnu $r(x)$ à partir de la liste des couples $(h_i(x),c_i(x))$. C'est à dire réussir à retrouver le polynôme aléatoire utilisé dans tous les chiffrements à partir des clés publiques $h_i(x)$ et des chiffrés $c_i(x)$.\\
Dans sa version décision, il s'agit de déterminer quels sont les couples d'une liste de $(h_i(x),c_i(x))$ dont les $c_i(x)$ sont générés aléatoirements et quels sont ceux qui ont été générés tel que décrits ci-dessus.\\
Comme nous l'avons évoqué rapidement en introduction de NTRU et NTRU$+$, nous pouvons relier ces deux problèmes à des problèmes de réseaux. En particulier des problèmes de type SVP (Short Vector Problem). Revenons d'abord sur la définition d'un réseau euclidien~:\\
Un réseau euclidien est un ensemble discret de points $\mathcal{L} \subset \mathbb{R}^n$ qui forme un sous-groupe pour l'addition, c'est-à-dire que pour tout $\mathbf{u}, \mathbf{v} \in \mathcal{L}$, le vecteur $\mathbf{u} + \mathbf{v}$ est également dans $\mathcal{L}$. Un réseau remplit l'espace, c'est à dire qu'il existe une constante positive $R > 0$ telle que tout point $\mathbf{x} \in \mathbb{R}^n$ peut être écrit de manière unique sous la forme $\mathbf{x} = \mathbf{u} + \mathbf{v}$, où $\mathbf{u} \in \mathcal{L}$ et $\| \mathbf{v} \| < R$ avec la norme euclidienne.\\
On peut plus généralement définir un réseau sur n'importe quel corps $K$ (dont les corps finis), $\mathcal{L} = \{\sum_{i=1}^{n} a_iv_i | a_i \in R\}$ avec $\{v_1,\cdots,v_n\}$ une $K$-base d'un $K$-espace vectoriel et $R$ un sous-anneau de $K$.
Le problème SVP consiste à trouver le plus petit vecteur $v$ non nul d'un réseau $\mathcal{L}$. On sait que ce problème est NP-difficile pour la norme euclidienne.



\section{Attaque}

Nous utiliserons la notation $e_i\in\{0,1\}^n$ où $(e_i)_i=1$ et $\forall j\neq i, (e_i)_j=0$, soit le vecteur dont la $i^\text{ème}$ coordonnée est $1$ et les autres $0$. Les polynômes de $\mathcal{R}$ seront assimilés à des vecteurs. Toutes les opérations scalaires sont dans $\mathbb{Z}/q\mathbb{Z}$ et les représentants sont pris dans $\left[-\frac{q}{2},\frac{q}{2}\right]$. Par exemple, dans $y\in\{-1,0,1\}, y\equiv g(x) \mod 3$, le modulo $3$ est appliqué au représentant de $g(x) \mod q$, ce qui se calcule avec l'opérateur $\rem$ (reste de la division euclidienne), 
$\begin{cases}
	y\equiv(g(x) \rem q) \mod 3 \text{ si } g(x)<\frac{q}{2}\\
	y\equiv(g(x) \rem q - q) \mod 3 \text{ sinon}
\end{cases}$.

\subsection{Théorie de l'attaque}

À l'instar de J.Lee et al.\cite{lee23}, nous allons montrer qu'il est possible de réaliser une attaque à chiffré choisi, cassant la prétendue sécurité OW-CCA de NTRU$+$.

\subsubsection{Non-injectivité de $\inv$}

Reprenons la définition de $\inv$~:

\begin{align*}
	\inv: \{-1,0,1\}^n \times \{0,1\}^{2n} \to& \{0,1\}^n \\
	(y, u) \mapsto& x\equiv(y+u_2) \oplus u_1 \mod 2 \\
	&\text{où } u=(u_1 , u_2) \text{ et } u_1,u_2\in \{0,1\}^n
\end{align*}

L'attaque repose sur la non-injectivité de cette fonction en $y$, que l'on peut démontrer facilement.
Soit $i\in\Iintv{1,n}$, si $y_i=-1$ et $(u_2)_i=(u_1)_i=1$, alors on a~:
\begin{align}
(y_i+(u_2)_i)\oplus(u_1)_i=0\oplus 1=1\equiv 1\mod 2
\end{align}

Maintenant, si ${y'}_i=y_i+2=1$, pour le même $u$ on a~:
\begin{align}
({y'}_i+(u_2)_i)\oplus(u_1)_i=2\oplus 1=3\equiv 1\mod 2
\end{align}

Nous avons donc $\inv(y,u)=\inv(y+2e_i,u)$ pour certaines valeurs de $y$.

Ce détail n'est pas explicité dans la description originale de l'algorithme.
Il était tout de même implémenté, le modulo $2$ étant codé par \verb+&1+, une opération qui ne conserve que le bit de poids faible et rend les autres bits nuls.

\subsubsection{Construire le bon $M'$}

Soit $c$ un chiffré. Pour exploiter la non-injectivité de $\inv$, on veut provoquer le calcul de $\inv(M+2e_i,*)$ avec le ième coefficient de M qui est à $-1$ (notons $\pi_i(M) = -1$) et ainsi retrouver un $r'$ égal à $r$ qui casse la sécurité que la transformation de Fujisaki-Okamoto est censée garantir.
Notons $c'$ le chiffré choisi par l'attaquant et $M'= M +2e_i$ la valeur que l'on souhaite donner à $\inv$ dans le $\decaps$ de l'oracle.
D'après la correctness de NTRU, si $M$ est déjà avec des coefficients plus petits que $3$ ie dans $\{-1,0,1\}$ (ce qui est le cas dans les situations légitimes) alors on sait que~:

\[
	M = c\cdot f \mod 3
\]

C'est à dire que si $M'\in\{-1,0,1\}$ on sait qu'il n'y aura pas d'erreur de déchiffrement lors du calcul de $c'\cdot f \mod 3$.\\
En modifiant le chiffré $c = h \cdot r + M$ en $c'=h \cdot r + M +2e_i$ de sorte que $M + 2e_i\in\{-1,0,1\}$ on aura bien le $M'$ voulu.
En effet, $c' - M' = c+2e_i - (M + 2e_i) = c - M $ ce qui implique que le $r$ retrouvé par $(c' - M')\cdot h^{-1}$ est le même qu'avec le chiffré légitime $c$.
Ainsi nous avons $\inv(M',G(r))$ qui redonne le même $m$ que $\inv(M',G(r))$ et donc $H(m)$ retourne le couple $(r,K)$ légitime.
On a bien été en mesure de produire un chiffré $c'$ que l'oracle accepte de déchiffrer afin de retrouver la clé encapsulée ie. on est en mesure de casser la One-Wayness en CCA.

Cependant, cette attaque repose sur l'hypothèse que $M'\in\{-1,0,1\}$ autrement dit que $\pi_i(M) = -1$, et ce n'est évidemment pas garanti. En fait, nous pouvons considérer 8 cas équiprobables qui décident de la valeur de $\pi_i(M)$. En prenant un indice $i$ quelconque, il y a 2 cas favorables sur les 8 qui donnent $\pi_i(M) = -1$ donc une probabilité de 1/4 que l'attaque réussisse.
Les 8 cas peuvent être résumés dans ce tableau en se rappelant comment est calculé $M$.

$$M=\sotp(m,(u_1,u_2))=(m\oplus u_1)-u_2$$
\begin{center}
\begin{tabular}{cc|c|c|c|c|}
	% \multicolumn{6}{c}{$M=\sotp(m,(u_1,u_2))=(m\oplus u_1)-u_2$} \\
	\cline{3-6}
	& & \multicolumn{4}{c|}{$(m,u_1)$} \\
	\cline{3-6}
	& & $(0,0)$ & $(0,1)$ & $(1,0)$ & $(1,1)$ \\
	\cline{1-6}
	\multicolumn{1}{|c}{\multirow{2}{*}{$u_2$}} & \multicolumn{1}{|c|}{$0$} & $0$ & $1$ & $1$ & $0$ \\
	\cline{2-6}
	\multicolumn{1}{|c}{} & \multicolumn{1}{|c|}{$1$} & $-1$ & $0$ & $0$ & $-1$ \\
	\cline{1-6}
\end{tabular}
\end{center}


\subsection{Contre-mesure}

Pour tous les chiffrés valides, dans $\inv$ on a $M+u_2\in\{0,1\}^n$, et l'attaque vise à produire un cas où $(M^*+u_2)_i=2$.
Pour rétablir l'injectivité de $\inv$ et empêcher l'attaque, il suffit donc de rejeter comme invalide un tel cas.
Voici la version corrigée~:

\begin{align*}
	\inv': \{-1,0,1\}^n \times \{0,1\}^{2n} \to& \{0,1\}^n\cup\{\bot\} \\
	(y, u) \mapsto& \begin{cases}
		x\equiv(y+u_2) \oplus u_1 \mod 2 &\text{ si } y+u_2\in\{0,1\}^n\\
		\bot &\text{ sinon}
	\end{cases}\\
	&\text{où } u=(u_1 , u_2) \text{ et } u_1,u_2\in \{0,1\}^n
\end{align*}

On pourrait s'inquiéter du fait que cette correction donne encore une information à l'attaquant~: un résultat $\bot$ pour un chiffré choisi pourrait donner des informations sur $f$. Cependant le test final $r\stackrel{?}{=}r'$ renvoie aussi $\bot$ en cas d'inégalité. L'attaquant ne peut donc pas distinguer la cause du rejet, et n'apprend rien de plus que l'invalidité du chiffré.

\section{Implémentation}

\subsection{Implémentation de référence en C}

La soumission de NTRU+ à la compétition KPQC était accompagnée d'une implémentation de référence écrite en C.\cite{ntruplusgit}
Le correctif proposé dans l'article de l'attaque a été enregistré le 10 juillet 2023 (commit \verb+6b6a476e+). Nous avons donc tenté de reproduire l'attaque sur la version précédente, du 25 mai 2023.

Dans la fonction \verb+poly_sotp_inv+, on retrouve bien le \verb+&1+ problématique~:

\begin{minted}{c}
t3 ^= (((e->coeffs[256*i + 16*l + 2*j + k] + t2)^t1) & 0x1) << (l+16*k);
\end{minted}

L'implémentation n'est pas aussi simple que la description mathématique de l'algorithme, à cause de problèmes liés au codage des polynômes en tableaux de bits et de la contrainte de l'exécution en temps constant.

Voici le code de l'attaque~:

\begin{minted}{c}
// Génération des clés
unsigned char pk[NTRUPLUS_PUBLICKEYBYTES];
unsigned char sk[NTRUPLUS_SECRETKEYBYTES];
crypto_kem_keypair(pk, sk);

// Encapsulation
unsigned char ciphertext[NTRUPLUS_CIPHERTEXTBYTES];
unsigned char shared_secret[NTRUPLUS_SSBYTES];
crypto_kem_enc(ciphertext, shared_secret, pk);

// Attaque
unsigned char ciphertext_prime[NTRUPLUS_CIPHERTEXTBYTES];
unsigned char shared_secret_prime[NTRUPLUS_SSBYTES];
poly c;
poly_frombytes(&c, ciphertext);
c.coeffs[0] += 2;
poly_tobytes(ciphertext_prime, &c);
crypto_kem_dec(shared_secret_prime, ciphertext_prime, sk);
\end{minted}

L'attaquant décode \verb+ciphertext+ en un polynôme \verb+c+, l'altère, puis l'encode en \verb+ciphertext_prime+ qu'il donne à l'oracle de déchiffrement.
Son but est d'obtenir l'égalité entre \verb+shared_secret+ et \verb+shared_secret_prime+, ce qui est vrai si \verb+crypto_kem_dec+ ne produit pas d'erreur.

Malheureusement nous n'avons pas atteint ce but, la décapsulation produisait toujours une erreur. Il est possible que ce soit dû à une subtilité dans le codage des polynômes ou la gestion des modulos.

\subsection{Notre implémentation en Python}

Afin de maîtriser tous les aspects du codage des polynômes, nous avons réimplementé NTRU+ $576$ en Python avec Sage.\cite{repo}

L'attaque sur cette version a fonctionné, avec en moyenne $25\%$ de réussite en modifiant un seul coefficient ce qui correspond à la probabilité de $1/4$ donnée dans la sous-section 4.1.2. En essayant $n$ coefficients, on atteint une probabilité de réussite $1-\left(\frac{3}{4}\right)^n$, soit $100\%$ en pratique avec $n=576$.

\subsection{Contre-mesure}

Le correctif a été ajouté à l'implémentation de référence (comme mentionné explicitement dans le document \textit{Supporting Documentation v2.0}, publié pour le deuxième tour du KPQC)\cite{ntruplusgit}.
Dans Inv, une variable accumule le OU de tous les bits indiquant une valeur illégale.
Cette variable n'est vérifiée qu'à la fin de la décapsulation, ce qui permet une exécution en temps constant.

Le temps constant empêche l'attaquant de déterminer si le rejet est dû à cette contre-mesure ou à la vérification de validité classique (ou l'indice de la coordonnée invalide), ce qui pourrait donner des informations sur le secret partagé.

\subsection{Exploitation en pratique\,?}

En considérant une utilisation de NTRU+ dans un protocole d'échange de clés réaliste, un attaquant pourrait-il utiliser cette attaque\,?
Comme nous l'avons vu précédemment, OW-CCA suppose l'accès à un oracle de déchiffrement acceptant des chiffrés arbitraires à l'exception de certains chiffrés valides connus.
Un tel oracle ne semble pas réaliste dans une application d'échange de clés.

Prenons un exemple d'utilisation simple de NTRU+ dans le cas d'une connexion sécurisée avec un serveur par Internet~:
Le client demande au serveur sa clé publique et la vérifie à l'aide d'un certificat.
Le client encapsule et envoie le chiffré au serveur, qui le décapsule.
Les deux ont désormais un secret partagé utilisable comme clé secrète pour du chiffrement symétrique.
Rien dans cet exemple n'est assimilable à un oracle de déchiffrement.

Prenons un autre exemple, cette fois un défi dont le but est de prouver sa connaissance de la clé privée afin de s'authentifier.
Le client envoie sa clé publique au serveur, qui encapsule le secret $m$ pour cette clé publique et répond le chiffré $c$.
Le client décapsule $c$ et renvoie le clair $m$ au serveur, qui peut vérifier sa validité.
Si le clair correspond bien au secret généré par l'encapsulation, alors le serveur sait que le client connaît la clé privée correspondant à la clé publique.
Notons que ce protocole nécessite que le message décapsulé soit envoyé par un canal sûr pour éviter une attaque Man in the Middle.
Supposons que le client refuse de répondre à un chiffré auquel il a déjà répondu, pour éviter une attaque par rejeu.
Si un attaquant connaît le chiffré $c$, il peut forger $c'$ et l'envoyer au client pour obtenir $m$, et ainsi usurper l'identité du client auprès du serveur.

On voit que la faille est exploitable pour ce protocole.
Il semble toutefois facile de le corriger~:
Plutôt que de comparer les chiffrés, le client devrait comparer les clairs, puisque la décapsulation n'est pas injective.
La communication entière devrait aussi s'effectuer dans un canal sûr et intègre, par exemple avec un échange de clés préalable avec la clé publique du serveur et du chiffrement symétrique.

Il semble donc que la possibilité d'exploiter cette faille dans un protocole vraisemblable suppose l'existence d'autres failles permettant des attaques plus simples.

\section{Conclusion}

Nous avons expliqué le fonctionnement du KEM NTRU+ et brièvement discuté sa sécurité. Sa première version contient une faille résidant dans la non-injectivité de l'algorithme $\inv$ et cassant la sécurité OW-CCA. L'attaque ne semble pas exploitable dans d'éventuelles utilisations concrètes. Un correctif simple et correct a tout de même été proposé dans l'article de 2023, et intégré dans une nouvelle version par l'équipe de NTRU+.

\newpage{}
\printbibliography[title={Bibliographie}]


\end{document}
