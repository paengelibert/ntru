#include "ntruplus576.h"
#include "rng.h"

#include <stdio.h>
#include <stdlib.h>

void help() {
	printf(
		"Usage: ntruplus576 g|e|d\n"
		"\n"
		"g: generate keys\n"
		"  stdout: (pk=%dB)(sk=%dB)\n"
		"\n"
		"e: encapsulate\n"
		"  stdin:  (pk=%dB)\n"
		"  stdout: (ciphertext=%dB)(shared_secret=%dB)\n"
		"\n"
		"d: decapsulate\n"
		"  stdin:  (ciphertext=%dB)(sk=%dB)\n"
		"  stdout: (shared_secret=%dB)\n",
		CRYPTO_PUBLICKEYBYTES, CRYPTO_SECRETKEYBYTES,
		CRYPTO_PUBLICKEYBYTES, CRYPTO_CIPHERTEXTBYTES, CRYPTO_BYTES,
		CRYPTO_CIPHERTEXTBYTES, CRYPTO_SECRETKEYBYTES, CRYPTO_BYTES
	);
}

int main(int argc, char *argv[]) {
	if(argc != 2) {
		help();
		return 1;
	}

	unsigned char entropy_input[48];
	FILE *random = fopen("/dev/random", "r");
	fread(entropy_input, 1, 48, random);
	fclose(random);
	randombytes_init(entropy_input, NULL, 256);

	if(argv[1][0] == 'g') {
		unsigned char pk[CRYPTO_PUBLICKEYBYTES];
		unsigned char sk[CRYPTO_SECRETKEYBYTES];
		crypto_kem_keypair(pk, sk);
		fwrite(pk, 1, CRYPTO_PUBLICKEYBYTES, stdout);
		fwrite(sk, 1, CRYPTO_SECRETKEYBYTES, stdout);
	}
	else if(argv[1][0] == 'e') {
		unsigned char pk[CRYPTO_PUBLICKEYBYTES];
		unsigned char ciphertext[CRYPTO_CIPHERTEXTBYTES];
		unsigned char shared_secret[CRYPTO_BYTES];
		fread(pk, 1, CRYPTO_PUBLICKEYBYTES, stdin);
		crypto_kem_enc(ciphertext, shared_secret, pk);
		fwrite(ciphertext, 1, CRYPTO_CIPHERTEXTBYTES, stdout);
		fwrite(shared_secret, 1, CRYPTO_BYTES, stdout);
	}
	else if(argv[1][0] == 'd') {
		unsigned char sk[CRYPTO_SECRETKEYBYTES];
		unsigned char ciphertext[CRYPTO_CIPHERTEXTBYTES];
		unsigned char shared_secret[CRYPTO_BYTES];
		fread(ciphertext, 1, CRYPTO_CIPHERTEXTBYTES, stdin);
		fread(sk, 1, CRYPTO_SECRETKEYBYTES, stdin);
		crypto_kem_dec(shared_secret, ciphertext, sk);
		fwrite(shared_secret, 1, CRYPTO_BYTES, stdout);
	}
	else {
		help();
		return 1;
	}
	return 0;
}
